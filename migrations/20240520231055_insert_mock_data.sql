-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS consumers (
    nik VARCHAR(32) PRIMARY KEY,
    full_name VARCHAR(100),
    legal_name VARCHAR(100),
    tempat_lahir VARCHAR(100),
    tanggal_lahir DATE,
    gaji INT,
    foto_ktp TEXT,
    foto_selfie TEXT,
    created_at TIMESTAMP NULL DEFAULT NOW(),
    updated_at TIMESTAMP NULL,
    deleted_at TIMESTAMP NULL DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS  limit_pinjaman (
    id SERIAL8,
    nik VARCHAR(32),
    tenor INT,
    limit_amount INT,
    created_at TIMESTAMP NULL DEFAULT NOW(),
    updated_at TIMESTAMP NULL,
    deleted_at TIMESTAMP NULL DEFAULT NULL,
    FOREIGN KEY (nik) REFERENCES consumers(nik)
);

CREATE TABLE IF NOT EXISTS transaction (
    id SERIAL8,
    nomor_kontrak VARCHAR(50),
    nik VARCHAR(32),
    otr INT,
    admin_fee INT,
    jumlah_cicilan INT,
    jumlah_bunga INT,
    nama_asset VARCHAR(100),
    created_at TIMESTAMP NULL DEFAULT NOW(),
    updated_at TIMESTAMP NULL,
    deleted_at TIMESTAMP NULL DEFAULT NULL,
    FOREIGN KEY (nik) REFERENCES consumers(nik)
);

INSERT INTO consumers (nik, full_name, legal_name, tempat_lahir, tanggal_lahir, gaji, foto_ktp, foto_selfie) 
VALUES (123456, 'Budi Balmond', 'Budi', 'Jakarta', '1970/01/01', 4000000, '', '');

INSERT INTO consumers (nik, full_name, legal_name, tempat_lahir, tanggal_lahir, gaji, foto_ktp, foto_selfie) 
VALUES (234567, 'Annisa Rafaela', 'Annisa', 'Jakarta', '1972/01/01', 6000000, '', '');

INSERT INTO limit_pinjaman  (nik, tenor, limit_amount) 
VALUES 
(123456, 1, 100000), (123456, 2, 200000), (123456, 3, 500000), (123456, 4, 700000),
(234567, 1, 1000000), (234567, 2, 1200000), (234567, 3, 1500000), (234567, 4, 2000000);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS public.`transaction`;
DROP TABLE IF EXISTS public.`limit_pinjaman`;
DROP TABLE IF EXISTS public.`consumers`;
-- +goose StatementEnd
