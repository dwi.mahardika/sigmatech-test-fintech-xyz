package utils

import (
	"fmt"
	"math"
	"strconv"
	"strings"
	"time"

	"github.com/dustin/go-humanize"
)

// Helper function to parse percentage string to float64
func ParsePercentage(value string) float64 {
	value = strings.TrimSuffix(value, "%")
	parsedValue, err := strconv.ParseFloat(value, 64)
	if err != nil {
		panic(fmt.Sprintf("Invalid percentage value: %s", value))
	}
	return parsedValue / 100
}

func RoundingPrice(priceBefore, unit float64) float64 {
	if math.Mod(priceBefore, unit) == 0 {
		return priceBefore
	}
	return (math.Floor(priceBefore/unit))*unit + unit
}

func FormatNumber(n float64) string {
	return humanize.Commaf(n)
}

func FormatTanggalLahir(t time.Time) string {
	return t.Format("02-01-2006")
}
