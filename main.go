package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"text/template"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"github.com/pressly/goose"
	"gitlab.com/dwi.mahardika/sigmatech-test-fintech-xyz/core"
	"gitlab.com/dwi.mahardika/sigmatech-test-fintech-xyz/handlers"
	"gitlab.com/dwi.mahardika/sigmatech-test-fintech-xyz/repositories"
	"gitlab.com/dwi.mahardika/sigmatech-test-fintech-xyz/utils"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

func init() {
	godotenv.Load()

}

func main() {
	DB_USER := os.Getenv("DB_USER")
	DB_PASSWORD := os.Getenv("DB_PASSWORD")
	DB_HOST := os.Getenv("DB_HOST")
	DB_PORT := os.Getenv("DB_PORT")
	DB_NAME := os.Getenv("DB_NAME")
	DB_OPTIONS := os.Getenv("DB_OPTIONS")

	dsn := fmt.Sprintf("postgres://%s:%s@%s:%s/%s%s", DB_USER, DB_PASSWORD, DB_HOST, DB_PORT, DB_NAME, DB_OPTIONS)
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Info),
	})
	if err != nil {
		log.Fatalf("Failed to connect to database: %v", err)
	}

	//migration data with goose, my fav tools :)
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalf("Failed to get sql.DB from GORM: %v", err)
	}
	runMigrations(sqlDB)

	consumerRepo := repositories.NewConsumerRepository(db)
	transactionRepo := repositories.NewTransactionRepository(db)
	limitRepo := repositories.NewLimitPinjamanRepository(db)

	consumerCore := core.NewConsumer(consumerRepo, limitRepo)
	trxCore := core.NewTransaction(transactionRepo, consumerRepo, limitRepo)

	r := gin.Default()

	funcMap := template.FuncMap{
		"formatNumber":       utils.FormatNumber,
		"formatTanggalLahir": utils.FormatTanggalLahir,
	}
	r.SetFuncMap(funcMap)
	r.LoadHTMLGlob("templates/*")
	r.Static("/static", "./static")
	r.Static("/uploads", "./assets/uploads")

	handlers.RegisterRoutes(r, consumerCore, trxCore)

	log.Println("Server started at :8080")
	if err := r.Run(":8080"); err != nil {
		log.Fatalf("Failed to start server: %v", err)
	}
}

func runMigrations(db *sql.DB) {
	// Set directory for migrations
	if err := goose.SetDialect("postgres"); err != nil {
		log.Fatalf("goose.SetDialect: %v", err)
	}

	migrationsDir := "./migrations"
	if err := goose.Up(db, migrationsDir); err != nil {
		log.Fatalf("goose.Up: %v", err)
	}
}
