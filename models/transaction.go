package models

import (
	"gorm.io/gorm"
)

type Transaction struct {
	gorm.Model
	NomorKontrak  string
	NIK           string   `gorm:"type:varchar(255)" validate:"required" json:"NIK"`
	OTR           float64  `validate:"required" json:"OTR"`
	AdminFee      float64  `json:"AdminFee"`
	JumlahCicilan int      `json:"JumlahCicilan"`
	JumlahBunga   float64  `json:"JumlahBunga"`
	NamaAsset     string   `validate:"required" json:"NamaAsset"`
	Consumer      Consumer `gorm:"foreignKey:NIK;references:NIK"`
}

func (Transaction) TableName() string {
	return "transaction"
}
