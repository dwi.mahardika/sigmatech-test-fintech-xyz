package models

import (
	"time"

	"gorm.io/gorm"
)

type Consumer struct {
	gorm.Model
	NIK                  string  `gorm:"type:varchar(255);unique"`
	FullName             string  `validate:"required"`
	LegalName            string  `validate:"required"`
	TempatLahir          string  `validate:"required"`
	TanggalLahir         string  `validate:"required"`
	Gaji                 float64 `validate:"required"`
	FotoKTP              string
	FotoSelfie           string
	LimitPinjaman        []LimitPinjaman `gorm:"foreignKey:NIK;references:NIK"`
	Transactions         []Transaction   `gorm:"foreignKey:NIK;references:NIK"`
	FormattedDateOfBirth string          `gorm:"-"`
}

func (c *Consumer) AfterFind(tx *gorm.DB) (err error) {
	dateOfBirth, err := time.Parse(time.RFC3339, c.TanggalLahir)
	if err != nil {
		return err
	}
	c.FormattedDateOfBirth = dateOfBirth.Format("02-01-2006")
	return nil
}
