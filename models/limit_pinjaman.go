package models

import (
	"gorm.io/gorm"
)

type LimitPinjaman struct {
	gorm.Model
	NIK         string
	Tenor       int
	LimitAmount float64
}

func (LimitPinjaman) TableName() string {
	return "limit_pinjaman"
}
