package core

import (
	"io"
	"mime/multipart"
	"os"

	"gitlab.com/dwi.mahardika/sigmatech-test-fintech-xyz/models"
	"gitlab.com/dwi.mahardika/sigmatech-test-fintech-xyz/repositories"
	"gitlab.com/rockondev/go-commons/logger"
)

type Consumer interface {
	GetAllConsumers() ([]models.Consumer, error)
	GetConsumerLimits(nik string) ([]models.LimitPinjaman, error)
	UpdateConsumerPhotos(nik, ktpPhoto, selfiePhoto string, file *multipart.FileHeader) error
}

type consumer struct {
	consumerRepo repositories.ConsumerRepository
	limitRepo    repositories.LimitPinjamanRepository
}

func NewConsumer(consumerRepo repositories.ConsumerRepository, limitRepo repositories.LimitPinjamanRepository) Consumer {
	return &consumer{consumerRepo, limitRepo}
}

func (u *consumer) GetAllConsumers() ([]models.Consumer, error) {
	consumers, err := u.consumerRepo.GetAll()
	if err != nil {
		logger.Error("err: ", err.Error())
		return nil, err
	}
	return consumers, nil
}

func (u *consumer) GetConsumerLimits(nik string) ([]models.LimitPinjaman, error) {
	return u.limitRepo.GetByNIK(nik)
}

func (u *consumer) UpdateConsumerPhotos(nik, ktpPhoto, selfiePhoto string, file *multipart.FileHeader) error {

	uploadDir := "static/uploads"
	if _, err := os.Stat(uploadDir); os.IsNotExist(err) {
		if err := os.MkdirAll(uploadDir, os.ModePerm); err != nil {
			logger.Error("err: ", err.Error())
			return err
		}
	}
	filePath := ""
	if ktpPhoto != "" {
		filePath = "static/uploads/" + nik + "-ktp.jpg"
	} else if selfiePhoto != "" {
		filePath = "static/uploads/" + nik + "-selfie.jpg"
	}

	fileHeader, err := file.Open()
	if err != nil {
		logger.Error("err: ", err.Error())
		return err
	}
	defer fileHeader.Close()

	destination, err := os.Create(filePath)
	if err != nil {
		logger.Error("err: ", err.Error())
		return err
	}
	defer destination.Close()

	_, err = io.Copy(destination, fileHeader)
	if err != nil {
		logger.Error("err: ", err.Error())
		return err
	}
	logger.Info("success copy file")

	switch true {
	case ktpPhoto != "":
		if err := u.consumerRepo.UpdateKTP(nik, filePath); err != nil {
			logger.Error("err: ", err.Error())
			return err
		}
		return nil
	case selfiePhoto != "":
		if err := u.consumerRepo.UpdateSelfie(nik, filePath); err != nil {
			logger.Error("err: ", err.Error())
			return err
		}
	}

	return nil
}
