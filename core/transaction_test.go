package core

import (
	"reflect"
	"testing"

	"gitlab.com/dwi.mahardika/sigmatech-test-fintech-xyz/models"
	"gitlab.com/dwi.mahardika/sigmatech-test-fintech-xyz/repositories"
)

func Test_transaction_CreateTransaction(t *testing.T) {
	type fields struct {
		transactionRepo repositories.TransactionRepository
		limitRepo       repositories.LimitPinjamanRepository
		consumerRepo    repositories.ConsumerRepository
	}
	type args struct {
		trx *models.Transaction
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tr := &transaction{
				transactionRepo: tt.fields.transactionRepo,
				limitRepo:       tt.fields.limitRepo,
				consumerRepo:    tt.fields.consumerRepo,
			}
			if err := tr.CreateTransaction(tt.args.trx); (err != nil) != tt.wantErr {
				t.Errorf("transaction.CreateTransaction() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_transaction_Calculate(t *testing.T) {
	type fields struct {
		transactionRepo repositories.TransactionRepository
		limitRepo       repositories.LimitPinjamanRepository
		consumerRepo    repositories.ConsumerRepository
	}
	type args struct {
		order *models.Transaction
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   int32
		want1  int32
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tr := &transaction{
				transactionRepo: tt.fields.transactionRepo,
				limitRepo:       tt.fields.limitRepo,
				consumerRepo:    tt.fields.consumerRepo,
			}
			got, got1 := tr.Calculate(tt.args.order)
			if got != tt.want {
				t.Errorf("transaction.Calculate() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("transaction.Calculate() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func Test_transaction_GetAllTransactions(t *testing.T) {
	type fields struct {
		transactionRepo repositories.TransactionRepository
		limitRepo       repositories.LimitPinjamanRepository
		consumerRepo    repositories.ConsumerRepository
	}
	tests := []struct {
		name    string
		fields  fields
		want    []models.Transaction
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tr := &transaction{
				transactionRepo: tt.fields.transactionRepo,
				limitRepo:       tt.fields.limitRepo,
				consumerRepo:    tt.fields.consumerRepo,
			}
			got, err := tr.GetAllTransactions()
			if (err != nil) != tt.wantErr {
				t.Errorf("transaction.GetAllTransactions() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("transaction.GetAllTransactions() = %v, want %v", got, tt.want)
			}
		})
	}
}
