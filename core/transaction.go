package core

import (
	"errors"
	"fmt"
	"log"
	"os"
	"sync"
	"time"

	"gitlab.com/dwi.mahardika/sigmatech-test-fintech-xyz/models"
	"gitlab.com/dwi.mahardika/sigmatech-test-fintech-xyz/repositories"
	"gitlab.com/dwi.mahardika/sigmatech-test-fintech-xyz/utils"
	"gitlab.com/rockondev/go-commons/logger"
)

type Transaction interface {
	CreateTransaction(transaction *models.Transaction) error
	Calculate(order *models.Transaction) (int32, int32)
	GetAllTransactions() ([]models.Transaction, error)
}

type transaction struct {
	transactionRepo repositories.TransactionRepository
	limitRepo       repositories.LimitPinjamanRepository
	consumerRepo    repositories.ConsumerRepository
}

func NewTransaction(transactionRepo repositories.TransactionRepository, consumerRepo repositories.ConsumerRepository, limitRepo repositories.LimitPinjamanRepository) Transaction {
	return &transaction{transactionRepo: transactionRepo, limitRepo: limitRepo, consumerRepo: consumerRepo}
}

var mutex = sync.Mutex{}

func (t *transaction) CreateTransaction(trx *models.Transaction) error {
	mutex.Lock()
	defer mutex.Unlock()
	consumerLimits, err := t.limitRepo.GetByNIK(trx.NIK)
	if err != nil {
		logger.Error("err: ", err.Error())
		return err
	}
	currentDate := time.Now().Format("02012006")
	contractNo := fmt.Sprintf("Contract-%s-%s", trx.NIK, currentDate)
	trx.NomorKontrak = contractNo

	for _, limit := range consumerLimits {
		if trx.OTR <= limit.LimitAmount && trx.JumlahCicilan == limit.Tenor {
			err := t.transactionRepo.CreateTransaction(trx)
			if err != nil {
				logger.Error("err: ", err.Error())
				return errors.New("gagal menyimpan ke database")
			}
			return nil
		}
	}
	return errors.New("limit exceeded")
}

func (t *transaction) Calculate(order *models.Transaction) (int32, int32) {

	adminFeePercentage := utils.ParsePercentage(os.Getenv("ADMIN_FEE"))
	loanInterestPercentage := utils.ParsePercentage(os.Getenv("LOAN_INTEREST"))

	log.Printf("adminFeePercent: %v, loanInterestPercent: %v", adminFeePercentage, loanInterestPercentage)
	adminFee := order.OTR * adminFeePercentage
	loanInterest := order.OTR * loanInterestPercentage
	adminFee = utils.RoundingPrice(adminFee, 1000)
	loanInterest = utils.RoundingPrice(loanInterest, 1000)
	return int32(adminFee), int32(loanInterest)

}

func (t *transaction) GetAllTransactions() ([]models.Transaction, error) {
	trx, err := t.transactionRepo.GetAll()
	if err != nil {
		logger.Error("err: ", err.Error())
		return nil, err
	}
	return trx, nil
}
