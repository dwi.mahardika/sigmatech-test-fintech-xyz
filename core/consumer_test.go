package core

import (
	"mime/multipart"
	"reflect"
	"testing"

	"gitlab.com/dwi.mahardika/sigmatech-test-fintech-xyz/models"
	"gitlab.com/dwi.mahardika/sigmatech-test-fintech-xyz/repositories"
)

func Test_consumer_GetAllConsumers(t *testing.T) {
	type fields struct {
		consumerRepo repositories.ConsumerRepository
		limitRepo    repositories.LimitPinjamanRepository
	}
	tests := []struct {
		name    string
		fields  fields
		want    []models.Consumer
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &consumer{
				consumerRepo: tt.fields.consumerRepo,
				limitRepo:    tt.fields.limitRepo,
			}
			got, err := u.GetAllConsumers()
			if (err != nil) != tt.wantErr {
				t.Errorf("consumer.GetAllConsumers() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("consumer.GetAllConsumers() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_consumer_GetConsumerLimits(t *testing.T) {
	type fields struct {
		consumerRepo repositories.ConsumerRepository
		limitRepo    repositories.LimitPinjamanRepository
	}
	type args struct {
		nik string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []models.LimitPinjaman
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &consumer{
				consumerRepo: tt.fields.consumerRepo,
				limitRepo:    tt.fields.limitRepo,
			}
			got, err := u.GetConsumerLimits(tt.args.nik)
			if (err != nil) != tt.wantErr {
				t.Errorf("consumer.GetConsumerLimits() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("consumer.GetConsumerLimits() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_consumer_UpdateConsumerPhotos(t *testing.T) {
	type fields struct {
		consumerRepo repositories.ConsumerRepository
		limitRepo    repositories.LimitPinjamanRepository
	}
	type args struct {
		nik         string
		ktpPhoto    string
		selfiePhoto string
		file        *multipart.FileHeader
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := &consumer{
				consumerRepo: tt.fields.consumerRepo,
				limitRepo:    tt.fields.limitRepo,
			}
			if err := u.UpdateConsumerPhotos(tt.args.nik, tt.args.ktpPhoto, tt.args.selfiePhoto, tt.args.file); (err != nil) != tt.wantErr {
				t.Errorf("consumer.UpdateConsumerPhotos() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
