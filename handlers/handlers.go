package handlers

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/dwi.mahardika/sigmatech-test-fintech-xyz/core"
	"gitlab.com/dwi.mahardika/sigmatech-test-fintech-xyz/models"
)

func RegisterRoutes(r *gin.Engine, consumers core.Consumer, transaction core.Transaction) {
	r.GET("/", HomePage)
	r.GET("/consumers", GetConsumersPage(consumers))
	r.GET("/consumers/:nik/limits", GetConsumerLimits(consumers))
	r.PUT("/consumers/:nik/upload/ktp", UploadKTP(consumers))
	r.PUT("/consumers/:nik/upload/selfie", UploadSelfie(consumers))
	r.GET("/calculate_transactions/:otr", CalculateTransaction(transaction))
	r.POST("/transaction", CreateTransaction(transaction))
	r.GET("/transactions", GetTransactionsPage(transaction))
}

func HomePage(c *gin.Context) {
	c.HTML(http.StatusOK, "index.html", gin.H{
		"title": "Home Page",
	})
}

func GetConsumersPage(consumersUseCase core.Consumer) gin.HandlerFunc {
	return func(c *gin.Context) {
		consumers, err := consumersUseCase.GetAllConsumers()
		if err != nil {
			c.HTML(http.StatusInternalServerError, "error.html", gin.H{"error": err.Error()})
			return
		}
		c.HTML(http.StatusOK, "consumers.html", gin.H{
			"title":     "List Consumers",
			"consumers": consumers,
		})
	}
}

func GetConsumerLimits(consumersUseCase core.Consumer) gin.HandlerFunc {
	return func(c *gin.Context) {
		nik := c.Param("nik")
		limits, err := consumersUseCase.GetConsumerLimits(nik)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		c.HTML(http.StatusOK, "limitPinjaman.html", gin.H{"limitPinjaman": limits})
	}
}

func UploadKTP(consumers core.Consumer) gin.HandlerFunc {
	return func(c *gin.Context) {
		nik := c.Param("nik")

		// Mengambil data KTP yang diunggah dari permintaan
		file, err := c.FormFile("fileInputKTP")
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		ktpName := file.Filename
		err = consumers.UpdateConsumerPhotos(nik, ktpName, "", file)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"message": "KTP file uploaded successfully "})
	}
}

func UploadSelfie(consumers core.Consumer) gin.HandlerFunc {
	return func(c *gin.Context) {
		nik := c.Param("nik")

		// Mengambil data KTP yang diunggah dari permintaan
		file, err := c.FormFile("fileInputSelfie")
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		selfieName := file.Filename
		err = consumers.UpdateConsumerPhotos(nik, "", selfieName, file)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}

		c.JSON(http.StatusOK, gin.H{"message": "Selfie file uploaded successfully"})
	}
}

func CalculateTransaction(transaction core.Transaction) gin.HandlerFunc {
	return func(c *gin.Context) {
		otrStr := c.Param("otr")
		var transactionModel models.Transaction
		otr, err := strconv.ParseFloat(otrStr, 64)
		if err != nil {
			log.Fatalf("err: %s", err.Error())
		}
		transactionModel.OTR = otr
		adminFee, jumlahBunga := transaction.Calculate(&transactionModel)
		c.JSON(http.StatusOK, gin.H{"admin_fee": adminFee, "jumlah_bunga": jumlahBunga})
	}
}

func CreateTransaction(transaction core.Transaction) gin.HandlerFunc {
	return func(c *gin.Context) {
		var transactionModel models.Transaction
		if err := c.ShouldBindJSON(&transactionModel); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		if err := transaction.CreateTransaction(&transactionModel); err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
		c.Status(http.StatusCreated)
	}
}

func GetTransactionsPage(transaction core.Transaction) gin.HandlerFunc {
	return func(c *gin.Context) {
		trx, err := transaction.GetAllTransactions()
		if err != nil {
			c.HTML(http.StatusInternalServerError, "error.html", gin.H{"error": err.Error()})
			return
		}
		c.HTML(http.StatusOK, "transaction.html", gin.H{
			"title":        "List Transactions",
			"transactions": trx,
		})
	}
}
