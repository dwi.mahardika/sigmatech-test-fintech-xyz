# sigmatech-test-fintech-xyz

## Getting started
Berikut langkah-langkah menggunakan aplikasi ini:
1. buatkan db xyz-finance pada postgresql (karena saya membuatnya untuk menggunakan postgresql)
2. copy file .env.example ke file .env contoh pada terminal: ```cp .env.example .env```
3. sesuaikan variable pada file .env dengan kebutuhan anda
4. lalu jalankan aplikasi dengan menggunakan syntax ```go run main.go``` pada terminal

Jika anda menggunakan docker, bisa anda build imagenya dengan perintah docker
```build -t sigmatech-finance:latest``` dan sebelum run pastikan environtment di isi semua karena bersifat mandatory, contohnya ada pada .env.example

contoh run aplikasi

```docker run -p 8080:8080 -e DB_HOST='localhost' -e DB_USER='postgres' -e DB_PASSWORD='postgres' -e DB_PORT='5432' -e DB_NAME='xyz-finance' -e DB_OPTIONS='?sslmode=disable' -e ADMIN_FEE='5%' -e LOAN_INTEREST='2%' sigmatech-test:latest```


## Architecture
Aplikasi ini menerapkan monolith sistem yang mana bisa dilihat untuk frontend dijalankan harus bersama dengan backend, template di dalam folder templates	/.
Aplikasi ini juga sudah menerapkan clean architecture, dimana syarat clean architecture adalah :  
1. Presentation Layer: Berisi HTTP handlers yang menerima dan memproses request dari pengguna. (Handler terpisah dengan logic, dan templates juga termasuk ini)
2. Application Layer: Berisi use cases yang merupakan logika bisnis utama. Ini mengacu pada folder core/ yang hanya berisi logic.
3. Domain Layer: Berisi entity dan repository interface. (model dan repository terpisah)
4. Infrastructure Layer: Berisi implementasi repository, konfigurasi database, dan utilitas lainnya. (handler, migrations db, config db, gcs, redis, kms terpisah - > belum sempat dibuat, hehe)

berikut desain architecture:  
![Image](architecture.drawio.png)

untuk struktur database berikut:   
![Image](erd.jpg)