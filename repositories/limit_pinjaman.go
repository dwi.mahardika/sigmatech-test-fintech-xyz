package repositories

import (
	"gitlab.com/dwi.mahardika/sigmatech-test-fintech-xyz/models"
	"gorm.io/gorm"
)

type LimitPinjamanRepository interface {
	GetByNIK(nik string) ([]models.LimitPinjaman, error)
}

type limitPinjamanRepository struct {
	db *gorm.DB
}

func NewLimitPinjamanRepository(db *gorm.DB) LimitPinjamanRepository {
	return &limitPinjamanRepository{db}
}

func (r *limitPinjamanRepository) GetByNIK(nik string) ([]models.LimitPinjaman, error) {
	var limitPinjaman []models.LimitPinjaman
	err := r.db.Where("nik = ?", nik).Find(&limitPinjaman).Error
	return limitPinjaman, err
}
