package repositories

import (
	"gitlab.com/dwi.mahardika/sigmatech-test-fintech-xyz/models"
	"gorm.io/gorm"
)

func NewConsumerRepository(db *gorm.DB) ConsumerRepository {
	return &consumerRepository{db}
}

type ConsumerRepository interface {
	GetAll() ([]models.Consumer, error)
	GetByNIK(nik string) (*models.Consumer, error)
	UpdateKTP(nik, ktpPhoto string) error
	UpdateSelfie(nik, selfiePhoto string) error
}

type consumerRepository struct {
	db *gorm.DB
}

func (r *consumerRepository) GetAll() ([]models.Consumer, error) {
	var consumers []models.Consumer
	err := r.db.Find(&consumers).Error
	return consumers, err
}

func (r *consumerRepository) GetByNIK(nik string) (*models.Consumer, error) {
	var consumer models.Consumer
	err := r.db.Where("nik = ?", nik).First(&consumer).Error
	return &consumer, err
}

func (r *consumerRepository) UpdateKTP(nik, ktpPhoto string) error {
	return r.db.Model(&models.Consumer{}).Where("nik = ?", nik).Updates(map[string]interface{}{
		"foto_ktp": ktpPhoto,
	}).Error
}

func (r *consumerRepository) UpdateSelfie(nik, selfiePhoto string) error {
	return r.db.Model(&models.Consumer{}).Where("nik = ?", nik).Updates(map[string]interface{}{
		"foto_selfie": selfiePhoto,
	}).Error
}
