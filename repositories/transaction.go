package repositories

import (
	"log"

	"gitlab.com/dwi.mahardika/sigmatech-test-fintech-xyz/models"
	"gorm.io/gorm"
)

type TransactionRepository interface {
	CreateTransaction(order *models.Transaction) error
	GetAll() ([]models.Transaction, error)
}

type transactionRepository struct {
	db *gorm.DB
}

func NewTransactionRepository(db *gorm.DB) TransactionRepository {
	return &transactionRepository{db}
}

func (r *transactionRepository) CreateTransaction(transaction *models.Transaction) error {
	err := r.db.Transaction(func(tx *gorm.DB) error {
		if err := tx.Create(&transaction).Error; err != nil {
			return err
		}
		return nil
	})
	return err
}

func (r *transactionRepository) GetAll() ([]models.Transaction, error) {
	var trx []models.Transaction
	if err := r.db.Preload("Consumer").Find(&trx).Error; err != nil {
		log.Fatalf("err: %s", err.Error())
		return nil, err
	}
	return trx, nil
}
