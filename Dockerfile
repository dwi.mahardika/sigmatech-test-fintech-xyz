FROM golang:alpine
WORKDIR /app
COPY . .
RUN go build -o main .

EXPOSE 8080

RUN mkdir -p /app/static/uploads
RUN chmod -R 777 /app/static/uploads

CMD ["./main"]