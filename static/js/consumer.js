document.addEventListener('DOMContentLoaded', function() {
    var buttonViewPinjaman = document.querySelectorAll('.btnViewLimitPinjaman');
    var btnKTP = document.querySelectorAll('.btnKTP');
    var btnSelfie = document.querySelectorAll('.btnSelfie');
    var btnTrx = document.querySelectorAll('.btnCreateTransaction');

    buttonViewPinjaman.forEach(function(button) {
        button.addEventListener('click', function() {
            var id = this.getAttribute('data-id');
            
            // Buat instance objek XMLHttpRequest
            var xhr = new XMLHttpRequest();

            // Atur callback ketika permintaan selesai
            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    document.getElementById("content-limit-pinjaman").innerHTML = xhr.responseText;
                }
            };

            xhr.open("GET", "/consumers/" + id + "/limits", true);
            xhr.send();
        });
    });


    btnKTP.forEach(function(button) {
        button.addEventListener('click', function() {
            var nik = this.getAttribute('data-id');
            var modalUploadKTP = document.getElementById('modalUploadKTP');
            modalUploadKTP.setAttribute('data-id', nik);
        });
    }); 

    btnSelfie.forEach(function(button) {
        button.addEventListener('click', function() {
            var nik = this.getAttribute('data-id');
            var modalUploadSelfie = document.getElementById('modalUploadSelfie');
            modalUploadSelfie.setAttribute('data-id', nik);
        });
    });

    btnTrx.forEach(function(button) {
        button.addEventListener('click', function() {
            var nik = this.getAttribute('data-id');
            var modalTrx = document.getElementById('modalCreateTransaction');
            var DataNIK = document.getElementById('ID_NIK');
            var modalTitle = document.querySelector('#modalCreateTransaction .modal-title');
            modalTitle.textContent = 'Create Transaction - ' + nik;
            modalTrx.setAttribute('data-id', nik);
            DataNIK.value = nik;
        });
    });

    var modalKTP = document.getElementById('modalUploadKTP');
    modalKTP.addEventListener('hidden.bs.modal', function() {
        var formKTP = document.getElementById('uploadFormKTP');
        formKTP.reset();

        var previewKTP = document.getElementById('previewKTP');
        previewKTP.innerHTML = '';
    });

    var modalSelfie = document.getElementById('modalUploadSelfie');
    modalSelfie.addEventListener('hidden.bs.modal', function() {
        var formSelfie = document.getElementById('uploadFormSelfie');
        formSelfie.reset();

        var previewSelfie = document.getElementById('previewSelfie');
        previewSelfie.innerHTML = '';
    });

    var modalCreateTransaction = document.getElementById('modalCreateTransaction');
    modalCreateTransaction.addEventListener('hidden.bs.modal', function() {
        var formTrx = document.getElementById('createTransaction');
        formTrx.reset();
    });

});

document.getElementById('fileInputKTP').addEventListener('change', function() {
    var file = this.files[0];
    var reader = new FileReader();

    reader.onload = function(event) {
        var preview = document.getElementById('previewKTP');
        preview.innerHTML = '';

        if (file.type.startsWith('image/')) {
            var img = document.createElement('img');
            img.src = event.target.result;
            img.className = 'img-fluid'
            img.alt = 'Preview Image';
            preview.appendChild(img);
        }
        else {
            var p = document.createElement('p');
            p.textContent = 'File type not supported';
            preview.appendChild(p);
        }
    };

    reader.readAsDataURL(file);
});

document.getElementById('fileInputSelfie').addEventListener('change', function() {
    var file = this.files[0];
    var reader = new FileReader();

    reader.onload = function(event) {
        var preview = document.getElementById('previewSelfie');
        preview.innerHTML = '';

        // Menampilkan gambar jika file adalah gambar
        if (file.type.startsWith('image/')) {
            var img = document.createElement('img');
            img.src = event.target.result;
            img.alt = 'Preview Image';
            img.className = 'img-fluid'
            preview.appendChild(img);
        } 
        // Menampilkan video jika file adalah video
        else if (file.type.startsWith('video/')) {
            var video = document.createElement('video');
            video.src = event.target.result;
            video.controls = true;
            preview.appendChild(video);
        } 
        // Menampilkan pesan jika file tidak didukung
        else {
            var p = document.createElement('p');
            p.textContent = 'File type not supported';
            preview.appendChild(p);
        }
    };

    reader.readAsDataURL(file);
});

document.getElementById('submitSelfie').addEventListener('click', function(e) {
    e.preventDefault()
    var modal = document.getElementById('modalUploadSelfie');
    var form = document.getElementById('uploadFormSelfie');
    var formData = new FormData(form);
    var nik = modal.getAttribute('data-id');
    var url = "/consumers/" + nik + "/upload/selfie";

    var xhr = new XMLHttpRequest();
    xhr.open('PUT', url, true);
    xhr.onload = function() {
        if (xhr.status === 200) {
            console.log('Upload successful');
        } else {
            console.error('Upload failed');
        }
    };
    xhr.send(formData);
    location.reload();
});

document.getElementById('submitKTP').addEventListener('click', function(e) {
    e.preventDefault()
    var modal = document.getElementById('modalUploadKTP')
    var form = document.getElementById('uploadFormKTP');
    var formData = new FormData(form);
    var nik = modal.getAttribute('data-id');
    var url = "/consumers/" + nik + "/upload/ktp";

    var xhr = new XMLHttpRequest();
    xhr.open('PUT', url, true);
    xhr.onload = function() {
        if (xhr.status === 200) {
            console.log('Upload successful');
        } else {
            console.error('Upload failed');
        }
    };
    xhr.send(formData);
    location.reload();
});

document.getElementById('kalkulasi').addEventListener('click', function(e) {
    e.preventDefault()
    var modal = document.getElementById('modalCreateTransaction');
    var form = document.getElementById('createTransaction');
    var otr = document.getElementById('OTR').value;
    var formData = new FormData(form);
    var nik = modal.getAttribute('data-id');
    var url = "/calculate_transactions/" + otr ;

    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.onload = function() {
        if (xhr.status === 200) {
            var response = JSON.parse(xhr.responseText);
            document.getElementById('AdminFee').value = response.admin_fee;
            document.getElementById('JumlahBunga').value = response.jumlah_bunga;
        } else {
            console.error('Failed calculate');
        }
    };
    xhr.send(formData);
});

document.getElementById('simpanTransaksi').addEventListener('click', function(e) {
    e.preventDefault();
    var form = document.getElementById('createTransaction');
    var formData = new FormData(form);
    var url = "/transaction";

    var object = {};
    formData.forEach(function(value, key) {
        // Convert appropriate fields to numbers
        if (key === "OTR" || key === "AdminFee" || key === "JumlahBunga") {
            object[key] = parseFloat(value);
        } else if (key === "JumlahCicilan") {
            object[key] = parseInt(value, 10);
        } else {
            object[key] = value;
        }
    });
    var json = JSON.stringify(object);

    var xhr = new XMLHttpRequest();
    xhr.open('POST', url, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onload = function() {
        if (xhr.status === 201) {
            alert("Berhasil membuat transaksi");
            location.reload();
        } else {
            console.error('Failed create transaction', xhr.responseText);
        }
    };
    xhr.send(json);
});